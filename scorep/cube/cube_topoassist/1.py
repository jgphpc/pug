#!/usr/bin/env python
# http://apprendre-python.com/page-xml-python-xpath
# https://bitbucket.org/jgphpc/pug/issues/59/topological-view

from lxml import etree
import sys, getopt

def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print 'test.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
   print 'Input file is', inputfile
   # print 'Output file is "', outputfile
   # tree = etree.parse("4c1cn.xml")
   tree = etree.parse(inputfile)
   for systemname in tree.xpath("/system/systemtreenode/systemtreenode/name"):
       print(systemname.text)
 
   #for user in tree.xpath("/system/systemtreenode/systemtreenode/name"):
   #    print(user.get("data-id"))
   

if __name__ == "__main__":
   main(sys.argv[1:])

# import sys
# print 'Number of arguments:', len(sys.argv), 'arguments.'
# print 'Argument List:', str(sys.argv), str(sys.argv[1])

#  #tree = etree.parse("data.xml")
#  #for user in tree.xpath("/users/user"):
#  #    print(user.get("data-id"))
#  
#  
